/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"
#include "helloworld-hard-keys.h"
#include "helloworld-hard-keys-window.h"
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#define WIN_WIDTH  480
#define WIN_HEIGHT 800

struct _HlwAppWindow
{
  GtkApplicationWindow parent;
  GtkWidget *label;
};

G_DEFINE_TYPE(HlwAppWindow, hlw_app_window, GTK_TYPE_APPLICATION_WINDOW);

static void
on_key_release_cb (GtkWidget *widget,
                   GdkEventKey *event,
                   gpointer user_data)
{
  GApplication *app = NULL;
  g_autofree gchar *text = NULL;

  HlwAppWindow *self  = HLW_APP_WINDOW (user_data);
  
  /* See gdk/gdkkeysyms.h for all keys */
  switch (event->keyval)
  {
    case GDK_KEY_q:
    case GDK_KEY_Escape:
      app = g_application_get_default();
      g_application_quit (app);
      break;
    case GDK_KEY_Right:
    case GDK_KEY_Up:
    case GDK_KEY_Forward:
    case GDK_KEY_Left:
    case GDK_KEY_Down:
    case GDK_KEY_Back:
      text = g_strdup_printf ("Pressed %s key\n",gdk_keyval_name (event->keyval));
      gtk_label_set_text(GTK_LABEL(self->label), text);
      break;
    default:
      break;
  }
}

static void
hlw_app_window_init (HlwAppWindow *app)
{
  HlwAppWindow *win = HLW_APP_WINDOW (app);
  gtk_window_set_default_size(GTK_WINDOW(win), WIN_WIDTH, WIN_HEIGHT);
  win->label = gtk_label_new("Press Navigation Keys");
  gtk_container_add(GTK_CONTAINER(win), win->label);
  g_signal_connect (win, "key-release-event",G_CALLBACK (on_key_release_cb), win);
  gtk_widget_show_all (GTK_WIDGET(win));
}

static void
hlw_app_window_class_init (HlwAppWindowClass *klass)
{
}

HlwAppWindow *
hlw_app_window_new (HlwApp *app)
{
  return g_object_new (HLW_APP_WINDOW_TYPE, "application", app, NULL);
}
